//Corbin Gomez & Weijie Zhang
#include <iostream>
#include "encoder.h"

using namespace std;

Encoder::Encoder()
{
} // Encoder()


Encoder::~Encoder()
{
} // ~Encoder()

void Encoder::encode(const unsigned char *message, const int size, 
  unsigned char *encodedMessage, 
    int *encodedSize)
{
	for(int i = 0; i < size; i++)
		encodedMessage[i] = '\0';
	readFile(message, size);
	*encodedSize = 0;
	writeKey(encodedMessage, encodedSize);
	writeEncodings(message, size, encodedMessage, encodedSize);
	//trie->LOprintTrie(trie->getRoot());
	//hash->printTable();

}//encode

/*
	Gets the bitNumth bit of num
*/
unsigned Encoder::getBit(unsigned int num, int bitNum)
{
	unsigned r = 0;
	r |= 1 << (bitNum - 1);
	r &= num;
	r >>= (bitNum - 1);
	return r;
}//getBit

/*
	Returns a group of bits
*/
unsigned Encoder::getBits(unsigned int num1, unsigned int num2, unsigned int code)
{
	unsigned r = 0;
	for(unsigned i = num1; i <= num2; i++)
		r |= 1 << i;

	return r & code;
}//getBits

/*
	Reads the message, and readys the hash and the trie for writing
*/
void Encoder::readFile(const unsigned char * message, const int size)
{
	hash = new CharNodeHash(256);
	hash->processStream(message, size);
	trie = new HuffmanTrie(hash, 256);
	trie->setBinaryLocation(trie->getRoot());
	trie->updateHashBinLoc(hash, trie->getRoot());
	//hash->setLeftJustified();
	//trie->LOprintTrie(trie->getRoot());
}//readFile

void Encoder::setBit(unsigned char & byte, int pos)
{
	byte |= 1 << (pos - 1);
}//setBit

/*
	Converts and writes each char into a binary equivalent
*/
void Encoder::writeEncodings(const unsigned char *message, const int size, 
    unsigned char *encodedMessage, int *encodedSize)
{
	//hash->printTable();
	hash->setLeftJustified();
	hash->generateVariations();
	hash->fillCodes();
	//hash->printTable();

	/*  //initializes all the chars in encodedMessage to 0
	for(int i = 0; i < size; i++)
	{
		encodedMessage[i] = '\0';
	}//for*/
	//cout << "Initial encoded Size: " << *encodedSize << endl;

	//setting the maximum size of the decoded message, for storage after the key:
	*((unsigned int *)&encodedMessage[1280]) = size;
	//cout << *((unsigned int *)&encodedMessage[1280]) << endl;
	int posI = 32; //keeps track of where we are in the int
	int posC = 8;  //keeps track of where we are in the char
	int windex = 1284; //write index, starting after the key
	int rindex = 0; //read index



	  //loop through all chars in the message
	while(rindex < size)
	{
		//cout << "Get's here! " << endl;
		//cout << "Processing: " << message[rindex] << endl;
		/*if(message[rindex] == '\0')
			break;*/
		  //loop through bits equivalent of the char at rindex
		for(unsigned int i = 0; i < hash->getLength(message[rindex]); i++)
		{
			  //if the bit is 1
			if(getBit(hash->getCode(message[rindex]), posI) == 1)
			{
				setBit(encodedMessage[windex], posC);
			}//if
			posI--;
			posC--;

			  //if we fill up the current char
			if(posC < 1)
			{
				windex++;
				(*encodedSize)++;
				posC = 8;
			}//if
		}//for
		rindex++;
		posI = 32;
	}//for

	/*if(posC < 8)
		(*encodedMessage)--;*/

	/*char wait;
	int pos = 0;
	int index = 1280;
	int counter = 0;
	int count = 1280;
	//cout << "Sample codes: " << hash->codes['e'][0] << endl;
	for(int i = 0; i < size; i++)
	{
		encodedMessage[ index + 0 ] |= ( (unsigned char*)&hash->codes[ message[i] ][ pos ] )[3];
		//cout << "encodedMessage[index + 0]: " << (unsigned int) encodedMessage[index + 0] << endl;
		encodedMessage[ index + 1 ] |= ( (unsigned char*)&hash->codes[ message[i] ][ pos ] )[2];
		//cout << "encodedMessage[index + 1]: " << (unsigned int) encodedMessage[index + 1] << endl;
		encodedMessage[ index + 2 ] |= ( (unsigned char*)&hash->codes[ message[i] ][ pos ] )[1];
		//cout << "encodedMessage[index + 2]: " << (unsigned int) encodedMessage[index + 2] << endl;
		encodedMessage[ index + 3 ] |= ( (unsigned char*)&hash->codes[ message[i] ][ pos ] )[0];
		//cout << "encodedMessage[index + 3]: " << (unsigned int) encodedMessage[index + 3] << endl;

		cout << "integer of encodedMessage[" << index << "]: " << *((unsigned int *)&encodedMessage[index]) << endl;
	
		cin>> wait;

		index = ((hash->getNode(message[i]).numBits + counter) / 8) + count;
		counter = counter + (hash->getNode(message[i]).numBits) % 8;
		pos = counter % 8;
	}//for
	encodedMessage[index + 1] = '\0';
	index++;
	encodedMessage[index + 1] = '\0';
	index++;
	encodedMessage[index + 1] = '\0';
	index++;
	encodedMessage[index + 1] = '\0';
	index++;
	*encodedSize = index;*/
}//writeEncodings

/*
	Writes the key to the encoded message
*/
void Encoder::writeKey(unsigned char *encodedMessage, int * encodedSize)
{
	  //for every character slot in the hash table
	for(int i = 0; i < 256; i++)
	{
		if(hash->getNode(i).isDefined)
		{
			//cout << i << "th Char: " << hash->getNode(i).c << "\t\t";

			*((unsigned int *)&encodedMessage[(*encodedSize)]) |= hash->getNode(i).binLoc;
			//cout << "Code: " << *((unsigned int *)&encodedMessage[(*encodedSize)]) << "\t\t(*encodedSize): " << (*encodedSize) << endl;
			(*encodedSize) += 4;

			encodedMessage[(*encodedSize)] = hash->getNode(i).numBits;
			//cout << "\t\tLength: " << (int)encodedMessage[(*encodedSize)] << "\t\t(*encodedSize): " << (*encodedSize) << endl;
			(*encodedSize)++;
		}//if

		else
		{
			(*encodedSize) += 5;
		}//else
	}//for

	//encodedMessage[50] = 'f';
}//writeFile
