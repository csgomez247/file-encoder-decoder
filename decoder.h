//Corbin Gomez & Weijie Zhang
#ifndef DECODER_H
#define	DECODER_H

struct TreeNode
{
	char c;
	bool isDefined;
	TreeNode * left;
	TreeNode * right;
};
/*struct Node
{
	char c;
	unsigned int code;
	Node * next;
};*/

class Decoder
{
public:
  Decoder();
  ~Decoder();
  void decode(const unsigned char* encodedMessage, const int encodedSize, 
    unsigned char* decodedMessage, int *decodedSize);
private:
	TreeNode * root;
	//char hash[65536];
	//Node outliers;

	void buildTree(const unsigned char * encodedMessage, const int encodedSize);
	void cleanBuffer(unsigned char buffer[32]);
	//void fillHash(const unsigned char * encodedMessage);
	char findChar(unsigned char buffer[32], int & pos, int & numUsed);
	unsigned getBit(unsigned int num, int bitNum);
	void loadBuffer(unsigned char buffer[32], unsigned int code);
	//TreeNode * getRoot();
	void placeNode(TreeNode * node, char myChar, unsigned int binLoc, int length, int start);
	void printTree(TreeNode * node);
	void writeDecodedMessage(const unsigned char* encodedMessage, const int encodedSize, 
    unsigned char* decodedMessage, int *decodedSize);

};

#endif	/* DECODER_H */

