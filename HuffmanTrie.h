//Corbin Gomez & Weijie Zhang
#ifndef HUFFMANTRIE_H
	#define HUFFMANTRIE_H

#include "BinaryHeap.h"
#include "CharNodeHash.h"
#include "QueueAr.h"

class HuffmanTrie
{
  private:
  	BinaryHeap<CharNode> heap;
  	int numElements;
  	CharNode const * root;

  	void generateTrie(int size);
    void deleteNodes(CharNode const * node);

  public:
  	HuffmanTrie(CharNodeHash * hash, int alphabetSize);
    ~HuffmanTrie();
  	int getBinaryEquivalent(char c);
    CharNode const * getRoot();
    void LOprintTrie(CharNode const * node);
  	void printSortedHeap();
    void printTrie(CharNode const * node);
    void setBinaryLocation(CharNode const * node);
    void shiftBitsLeft(CharNode * node);
    void shiftLeftAndOr(CharNode * node);
    void updateHashBinLoc(CharNodeHash * hash, CharNode const * node);


};//HuffmanTrie

#endif