//Corbin Gomez & Weijie Zhang
#ifndef ENCODER_H
#define	ENCODER_H

#include "CharNodeHash.h"
#include "HuffmanTrie.h"

class Encoder
{
public:
  Encoder();
  void encode(const unsigned char *message, const int size, 
    unsigned char *encodedMessage, int *encodedSize);
  ~Encoder();
  unsigned getBit(unsigned int num, int bitNum);
  unsigned getBits(unsigned int num1, unsigned int num2, unsigned int code);
  void readFile(const unsigned char * message, const int size);
  void setBit(unsigned char & byte, int pos);
  void writeEncodings(const unsigned char *message, const int size, 
    unsigned char *encodedMessage, int *encodedSize);
  void writeKey(unsigned char * encodedMessage, int * encodedSize);
private:
	HuffmanTrie * trie;
	CharNodeHash * hash;
};

#endif	/* ENCODER_H */

