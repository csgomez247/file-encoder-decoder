//Corbin Gomez & Weijie Zhang

#include "decoder.h"
#include <iostream>

using namespace std;

Decoder::Decoder()
{
	root = new TreeNode;
	root->c = 0;
	root->isDefined = false;
	root->left = 0;
	root->right = 0;
} // Decoder()


Decoder::~Decoder()
{
} // ~Decoder()

void Decoder::buildTree(const unsigned char * encodedMessage, const int encodedSize)
{
	int index = 0;
	char currChar;

	while(index < 1280)
	{
		//cout << "Get's here!" << endl;
		  //if the encodedMessage holds a code
		if((int)encodedMessage[index + 4] > 0)
		{	
			//cout << "Get's here!" << endl;
			currChar = index / 5;
			int start = 0;
			//cout << "code for " << currChar << ": " << *((unsigned int *)&encodedMessage[index]) << "\tlength: " << (unsigned int)encodedMessage[index + 4] << endl;
			placeNode(root, currChar, *((unsigned int *)&encodedMessage[index]), (unsigned int)encodedMessage[index + 4], start);
			//cout << "Placing a new char." << endl;
		}//if
		//increment 5 spots up
		index += 5;
	}//while
}//buildTree

void Decoder::cleanBuffer(unsigned char buffer[32])
{
	for(int i = 0; i < 32; i++)
	{
		buffer[i] = '\0';
	}//for
}//cleanBuffer

void Decoder::decode(const unsigned char* encodedMessage, const int encodedSize, 
  unsigned char* decodedMessage, int *decodedSize)
{
	buildTree(encodedMessage, encodedSize);
	//printTree(root);
	writeDecodedMessage(encodedMessage, encodedSize, decodedMessage, decodedSize);
	//fillHash(encodedMessage);
} // decode()

/*
	Fill the hash table with the codes and characters
*/
/*void Decoder::fillHash(const unsigned char * encodedMessage)
{
	int index = 0;
	char currChar;
	while(index < 1280)
	{
		  //if the encodedMessage holds a code
		if(encodedMessage[index] != 0)
		{
			currChar = index / 5;
			hash[*((unsigned int *)&encodedMessage[index])] = currChar;
		}//if
		index += 5;
	}
}//fillHash*/

char Decoder::findChar(unsigned char buffer[32], int & pos, int & numUsed)
{
	/*if(node)
	{
		if(getBit(code, 32) == 0)
		{
			code <<= 1;
			findChar(node->left, code);
		}//if
		if(getBit(code,32) == 1)
		{
			code <<= 1;
			findChar(node->right, code);
		}//if
	}//else*/
	numUsed = 0;;
	TreeNode * node = root;
	while(node->right && node->left)
	{
		if(buffer[numUsed] == '0')
		{
			node = node->left;
		}//if
		if(buffer[numUsed] == '1')
		{
			node = node->right;
		}//if
		pos++;
		numUsed++;
	}//while

	return node->c;
}//findChar

/*
	Gets the bitNumth bit of num
*/
unsigned Decoder::getBit(unsigned int num, int bitNum)
{
	unsigned r = 0;
	r |= 1 << (bitNum - 1);
	r &= num;
	r >>= (bitNum - 1);
	return r;
}//getBit

void Decoder::loadBuffer(unsigned char buffer[32], unsigned int code)
{
	for(int i = 32; i >= 1; i--)
	{
		buffer[32 - i] = '0' + getBit(code, i);
	}//for
}//loadBuffer

void Decoder::placeNode(TreeNode * node, char myChar, unsigned int binLoc, int length, int start)
{
	if(start < length)
	{
		//cout << "binloc of " << myChar<< ": " << binLoc<<"\t"<< getBit(binLoc, (length - start)) << endl;
		if(getBit(binLoc, (length - start)) == 0)
		{
			if(!node->left)
			{
				node->left = new TreeNode;
				node->left->left = 0;
				node->left->right = 0;
				node->isDefined = false;
			}//if
			//cout << "Going left!" << endl;
			placeNode(node->left, myChar, binLoc, length, (start + 1));
		}//if

		if(getBit(binLoc, (length - start)) == 1)
		{
			if(!node->right)
			{
				node->right = new TreeNode;
				node->right->left = 0;
				node->right->right = 0;
				node->isDefined = false;
			}//if
			//cout << "Going right!" << endl;
			placeNode(node->right, myChar, binLoc, length, (start + 1));
		}//else
	}//if
	else
	{
		/*if(myChar == 'O')
			cout << "Why is this here?" << endl;*/
		node->c = myChar;
		node->isDefined = true;
		//cout << "Placing " << node->c << endl;
		return;
	}//else

}//placeNode

void Decoder::printTree(TreeNode * node)
{
	if(node)
	{
		//cout << "Going Left:" << endl;
		printTree(node->left);

		if(!node->left && !node->right)
			//cout << "Here is the char: " << node->c << endl;

		//cout << "Going Right: " << endl;
		printTree(node->right);
	}
}//printTree

void Decoder::writeDecodedMessage(const unsigned char* encodedMessage, const int encodedSize, 
    unsigned char* decodedMessage, int *decodedSize)
{

	/*for(int i = 8; i > 0; i--)
		cout << getBit(encodedMessage[1281], i);
	cout << endl;*/

	TreeNode * node = root;
	int pos = 8;
	int index = 1284;
	int windex = 0;
	unsigned direction = 0;
	int originalMessageSize = *((unsigned int *)&encodedMessage[1280]);
	//cout << originalMessageSize << endl;

	while(index < originalMessageSize)
	{
		if(pos < 1)
		{
			index++;
			pos = 8;
		}//if

		direction = getBit(encodedMessage[index], pos);

		if(direction == 0)
		{
			node = node->left;

			  //if its a leaf
			if(node->isDefined)
			{
				decodedMessage[windex] = node->c;
				//cout << "Successful write: " << decodedMessage[windex] << endl;
				windex++;
				//(*decodedSize)++;
				node = root;
			}//if
		}//if
		else//if direction == 1
		{
			node = node->right;

			  //if its a leaf
			if(node->isDefined)
			{
				decodedMessage[windex] = node->c;
				//cout << "Successful write: " << decodedMessage[windex] << endl;
				windex++;
				//(*decodedSize)++;
				node = root;
			}
		}//else

		pos--;
	}//while
	//decodedMessage[windex - 1] = '\0';
	*decodedSize = index;

	/*char buffer[32];

	loadBuffer(buffer, encodedMessage[index])

	bool finding = true;

	while(finding)
	{

	}*/

	/*cout << "Here is the message: " << endl << endl;
	for(int i = 0; i < windex; i++)
	{
		cout << decodedMessage[i];
	}//for*/

	//cout << endl << endl;
	//cout << "Decoded size: " << *decodedSize << endl;
	//cout << "Windex: " << windex << endl;
	//cout << "Pos: " << pos << endl;

	/*cout << getBit(encodedMessage[1280], 8) << endl;
	cout << getBit(encodedMessage[1280], 7) << endl;
	cout << getBit(encodedMessage[1280], 6) << endl;
	cout << getBit(encodedMessage[1280], 5) << endl;
	cout << getBit(encodedMessage[1280], 4) << endl;
	cout << getBit(encodedMessage[1280], 3) << endl;
	cout << getBit(encodedMessage[1280], 2) << endl;
	cout << getBit(encodedMessage[1280], 1) << endl;*/

	/*unsigned char buffer[32];
	//bool more = false;
	int pos = 0;
	int numUsed = 0;
	int index = 1280;
	int bitSpace = 0;
	CharQueue q;
	int writeIndex = 0;


	cout << "\"1st\" byte: " << (unsigned int)encodedMessage[1280] << endl;
	cout << "\"2nd\" byte: " << (unsigned int)encodedMessage[1281] << endl;
	cout << "\"3rd\" byte: " << (unsigned int)encodedMessage[1282] << endl;
	cout << "\"4th\" byte: " << (unsigned int)encodedMessage[1283] << endl;
	cout << "\"5th\" byte: " << (unsigned int)encodedMessage[1284] << endl;
	
	  //Queues the first 64 bits of the encodedMessage
	while(index <= encodedSize && bitSpace < 64)
	{
		loadBuffer(buffer, encodedMessage[index] << 24);
		for(int i = 0; i < 8; i++)
		{
			q.enqueue(buffer[i]);
			pos++;
			cout << q.getElement(i + bitSpace);
		}//for
		index += pos / 8;
		bitSpace += pos;
		pos %= 8;
		cout << endl;
	}
	cleanBuffer(buffer);

	cout <<"Queue: ";	
	q.printQueue();


	while(q.getSize() > 0)
	{
		cout <<"Storing the first 32 bits in the buffer...." << endl;
		for(int i = 0; i < 32; i++)
		{
			buffer[i] = q.getElement(i);
			//q.dequeue();
		}//for

		cout << "Buffer: ";
		for(int i = 0; i < 32; i++)
		{
			cout << buffer[i];
		}//for
		cout << endl;

		if(!q.hasValidChars())
		break;
		char poop = findChar(buffer, pos, numUsed);
		cout << "First char of buffer: " << poop << endl;
		cout << "Pos: " << pos << endl;

		cout << "Putting the " << writeIndex << "th char into the decodedMessage..." << endl;
		decodedMessage[writeIndex] = poop;
		writeIndex++;

		
		cout << "Dequeueing the " << numUsed << " used bits..." <<endl;
		for(int i = 0; i < numUsed; i++)
		{
			q.dequeue();
			bitSpace--;
		}//for
		cout << "Queue: ";
		q.printQueue();
		cout << "bitSpace: " << bitSpace << endl;
		cout << "Queue size: " << q.getSize() << endl;

		if(q.getSize() <= 32 && index < encodedSize)
		{
			cout << "Enqueueing the next " << pos << " available bits..." << endl;
			cleanBuffer(buffer);
			while(index <= encodedSize && bitSpace < 64)
			{
				loadBuffer(buffer, encodedMessage[index] << 24);
				for(int i = 0; i < pos; i++)
				{
					q.enqueue(buffer[i]);
					cout << q.getElement(i + bitSpace);
				}//for
				index += pos / 8;
				bitSpace += pos;
				pos %= 8;
				cout << endl;
				cout << "index: " << index << endl;
				cout << "bitSpace: " << bitSpace << endl;
				cout << "Pos: " << pos << endl;
			}//while
		}//if
		cleanBuffer(buffer);

		cout <<"Queue: ";	
		q.printQueue();

		cout << endl << endl;

	}//while

	*decodedSize = writeIndex;

	cout << "The decodedMessage: ";
	for(int i = 0; i < *decodedSize; i++)
	{
		cout << decodedMessage[i];
	}//for
	cout << endl;*/

}//writeDecodedMessage
