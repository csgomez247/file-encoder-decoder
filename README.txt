Corbin Gomez

File Encoder / Decoder
----------------------
Compiles with 'make'
Run executable with './encoder.out <file>' where <file> is a file to be encoded and decoded

Program outputs a run time, the amount of space used, and a possible mismatch message.
