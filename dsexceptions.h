//Corbin Gomez & Weijie Zhang
        #ifndef _DSEXCEPTIONS_H_
        #define _DSEXCEPTIONS_H_

        class Underflow { };
        class Overflow  { };
        class OutOfMemory { };
        class BadIterator { };

        #endif
