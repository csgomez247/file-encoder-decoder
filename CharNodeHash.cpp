//Corbin Gomez & Weijie Zhang
#include "CharNodeHash.h"
#include <iostream>

using namespace std;

/*
	Makes the hash table
*/	
CharNodeHash::CharNodeHash(int size)
{
	charHash = new CharNode[size];
	tableSize = size;
	initializeCharNodes();
}//constructor

/*
	Destructs the hash table
*/
CharNodeHash::~CharNodeHash()
{
	cout << "Deleting the hash table..." << endl;
	delete[] charHash;
}//destructor

void CharNodeHash::fillCodes()
{
	  //clean up codes
	for(int i = 0; i < 256; i++)
	{
		for(int j = 0; j < 8; j++)
			codes[i][j] = 0;
	}//for


	for(int i = 0; i < 256; i++)
	{
		if(charHash[i].isDefined)
		{
			for(int j = 0; j < 8; j++)
			{
				codes[i][j] = getVariation(charHash[i].c, j);
				//cout << j << "th variation of " << charHash[i].c << ": " << codes[i][j] << endl;
			}//for
		}//if
	}//for
}//fillCodes

/*
	Generates 8 variations on the code
*/
void CharNodeHash::generateVariations()
{
	  //loop through all the elements
	for(int i = 0; i < 256; i++)
	{
		if(charHash[i].isDefined)
		{
			  //set each variation of the code
			for(int j = 0; j < 8; j++)
			{
				charHash[i].variations[j] = charHash[i].binLoc >> j;
				//cout << j << "th variation on " << charHash[i].c << ": " << charHash[i].variations[j] << endl;
			}//for
		}//if
	}//for
}

/*
	Gets the code of a specific char
*/
unsigned int CharNodeHash::getCode(char c)
{
	return charHash[hashVal(c)].binLoc;
}//getCode

unsigned int CharNodeHash::getLength(char c)
{
	return charHash[hashVal(c)].numBits;
}

/*
	Access a node of a specific char
*/
CharNode CharNodeHash::getNode(char c)
{
	return charHash[hashVal(c)];
}//getNode

/*
	Access the node of a specific index
*/
CharNode CharNodeHash::getNode(int index)
{
	return charHash[index];
}//getNode

/*
	Returns the numVarth variation on c
*/
unsigned int CharNodeHash::getVariation(char c, int numVar)
{
	return charHash[hashVal(c)].variations[numVar];
}//getVariation

/*
	Computes the array index of each key
*/
int CharNodeHash::hashVal(char c)
{
	return (int)((unsigned char) c);
}//hashVal

/*
	Marks the CharNodes as undefined and sets the frequency to 0
*/
void CharNodeHash::initializeCharNodes()
{
	  //loop and change isDefineds and frequencies to false, 0
	for(int i = 0; i < tableSize; i++)
	{
		(charHash[i]).isDefined = false;
		(charHash[i]).frequency = 0;
		(charHash[i]).left = NULL;
		(charHash[i]).right = NULL;
		(charHash[i]).binLoc = 0;
		(charHash[i]).numBits = 0;
	}//for
}//initializeCharNodes

/*
	Checks to see if CharNode is defined
*/
bool CharNodeHash::isDefined(int index)
{
	return charHash[index].isDefined;
}

/*
	Accesses CharNode and increments the frequency
*/
void CharNodeHash::processStream(const unsigned char * stream, int size)
{
	  //loop through all characters 
	for(int i = 0; i < size; i++)
	{
		int pos = hashVal(stream[i]);
		  //if charnode already has a defined character
		if(charHash[pos].isDefined)
			(charHash[pos].frequency)++;
		else//initialize character, increment frequency, mark defined
		{
			charHash[pos].isDefined = true;
			charHash[pos].c = stream[i];
			(charHash[pos].frequency)++;
		}//else
	}//for
}//insert

/*
	Prints CharNode
*/
void CharNodeHash::printNode(char c)
{
	CharNode node = getNode(hashVal(c));
	cout << node.c << "\t" << node.frequency << endl;
}

/*
	Prints table
*/
void CharNodeHash::printTable()
{
	  //loop to print
	for(int i = 0; i < tableSize; i++)
	{
		cout << i << "\t";
		if(getNode(i).isDefined)
			cout << charHash[i].c << "\t" << charHash[i].binLoc << "\t" << charHash[i].numBits << endl;
		else
			cout << endl;
	}//for
}//print


/*
	Shifts all binLocs to have the first significant bit at the very left
*/
void CharNodeHash::setLeftJustified()
{
	  //loop through elements
	for(int i = 0; i < 256; i++)
	{
		if(charHash[i].isDefined)
			charHash[i].binLoc <<= (sizeof(unsigned int) * 8 - charHash[i].numBits);
	}//for
}//setLeftJustified

/*
	Updates the binLoc in each valid CharNode
*/
void CharNodeHash::updateCharNode(char c, unsigned int loc, unsigned int bits)
{
	charHash[hashVal(c)].binLoc = loc;
	charHash[hashVal(c)].numBits = bits;
}//updateCharNode

/*
	Friendly overloaded >
*/
bool operator> (const CharNode & c1, const CharNode & c2)
{
	return c1.frequency > c2.frequency;
}//operator >

/*
	Friendly overloaded <
*/
bool operator< (const CharNode & c1, const CharNode & c2)
{
	return c1.frequency < c2.frequency;
}//operator<

/*
	Friendly overloaded >=
*/
bool operator>= (const CharNode & c1, const CharNode & c2)
{
	/*if(c1.frequency == c2.frequency)
		return (int)((unsigned char)c1.c) > (int)((unsigned char)c2.c);*/
	return c1.frequency >= c2.frequency;
}//operator>=

/*
	Friendly overloaded <=
*/
bool operator<= (const CharNode & c1, const CharNode & c2)
{
	  //if the frequencies are the same
	/*if(c1.frequency == c2.frequency)
		return (int)((unsigned char)c1.c) < (int)((unsigned char)c2.c);*/
	return c1.frequency <= c2.frequency;
}//operator<=

/*
	Friendly overloaded ==
*/
bool operator== (const CharNode & c1, const CharNode & c2)
{
	  //if the frequencies tie,
	/*if(c1.frequency == c2.frequency)
	{
		return (int)((unsigned char)c1.c) == (int)((unsigned char)c2.c);
	}//if*/
	return c1.frequency == c2.frequency;
}//operator==

/*
	Friendly overloaded !=
*/
bool operator!= (const CharNode & c1, const CharNode & c2)
{
	return c1.frequency != c2.frequency;
}//operator!=