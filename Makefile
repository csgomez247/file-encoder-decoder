#Corbin Gomez & Weijie Zhang
CC=g++
CFLAGS=-Wall -ansi -g

TARGET=encoder.out
HEADERS=decoder.h encoder.h CharNodeHash.h BinaryHeap.h HuffmanTrie.h vector.h QueueAr.h dsexceptions.h 
SOURCES=decoder.cpp encoder.cpp CharNodeHash.cpp HuffmanTrie.cpp vector.cpp encoderRunner.cpp

$(TARGET): $(SOURCES) $(HEADERS) 		
	$(CC) $(CFLAGS) -o $(TARGET) $(SOURCES)

clean:
	rm -f $(TARGET) 
