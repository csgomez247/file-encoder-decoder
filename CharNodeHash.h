//Corbin Gomez & Weijie Zhang
#ifndef CHARNODEHASH_H
	#define CHARNODEHASH_H

struct CharNode
{
  bool isDefined;
	char c;
	unsigned int frequency;
	unsigned int variations[8];
  CharNode * left;
  CharNode * right;
  unsigned int binLoc;
  unsigned int numBits;
  friend bool operator> (const CharNode & c1, const CharNode & c2);
  friend bool operator< (const CharNode & c1, const CharNode & c2);
  friend bool operator>= (const CharNode & c1, const CharNode & c2);
  friend bool operator<= (const CharNode & c1, const CharNode & c2);
  friend bool operator== (const CharNode & c1, const CharNode & c2);
  friend bool operator!= (const CharNode & c1, const CharNode & c2); 
};

class CharNodeHash
{
  private:
  	CharNode * charHash;
  	int tableSize;
    

    void initializeCharNodes();

  public:
    unsigned int codes[256][8];

  	CharNodeHash(int size = 256);
  	~CharNodeHash();
    void fillCodes();
    void generateVariations();
    unsigned int getCode(char c);
    unsigned int getLength(char c);
    CharNode getNode(char c);
    CharNode getNode(int i);
    unsigned int getVariation(char c, int numVar);
  	int hashVal(char c);
    bool isDefined(int index);
    void setLeftJustified();
    void processStream(const unsigned char * stream, const int size);
    void printNode(char c);
    void printTable();
    void updateCharNode(char c, unsigned int loc, unsigned int bits);
};//HashCharNode 

#endif