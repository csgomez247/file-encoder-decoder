//Corbin Gomez & Weijie Zhang
#include "HuffmanTrie.h"
#include <iostream>

using namespace std;

void wait()
{
	char wait;
	cin >> wait;
}

/*
	Constructs a HuffmanTrie
*/
HuffmanTrie::HuffmanTrie(CharNodeHash * hash, int alphabetSize)
{
	heap.makeEmpty();
	numElements = 0;
	  //loop through all defined nodes and add to heap
	for(int i = 0; i < alphabetSize; i++)
	{
		  //if the ith char is in the file
		if(hash->isDefined(i))
		{	
			numElements++;
			heap.insert(hash->getNode(i));
		}//if
	}//for

	root = NULL;

	generateTrie(numElements);
}//constructor

/*
	Destrucs the HuffmanTrie (allocated charNodes)
*/
HuffmanTrie::~HuffmanTrie()
{
	deleteNodes(root);
	cout << "Deleting trie...\n";
}//destructor

/*
	Recurses through trie, deleting all of the allcoated CharNodes
*/
void HuffmanTrie::deleteNodes(CharNode const * node)
{
	//Base Case
	if(!node)
		return;
	else//delete the CharNodes
	{
		deleteNodes(node->left);
		deleteNodes(node->right);
		
		delete node->left;
		delete node->right;
	}//else
}//deleteNodes

/*
	Generates trie
*/
void HuffmanTrie::generateTrie(int size)
{
	  //while the heap has more than one node.
	while(size > 1)
	{
		CharNode temp1 = heap.findMin();
		heap.deleteMin();
		size--;

		CharNode temp2 = heap.findMin();
		heap.deleteMin();
		size--;

		CharNode internalNode;
		internalNode.isDefined = false;
		internalNode.binLoc = 0;
		internalNode.frequency = temp1.frequency + temp2.frequency;

		internalNode.left = new CharNode(temp1);
		internalNode.right = new CharNode(temp2);

		heap.insert(internalNode);
		size++;
	}//while
	root = &heap.findMin();
}//generateTrie

/*
	Traverses the tree to find binary equivalents
*/
int getBinaryEquivalent(char c)
{
	return 0;
}//getBinary Equivalent

/*
	Returns the root of the trie
*/
CharNode const * HuffmanTrie::getRoot()
{
	return root;
}//getRoot()

/*
	Level Order print of trie
*/
void HuffmanTrie::LOprintTrie(CharNode const * root)
{
	//int location = 0;
	Queue<CharNode> myQueue(numElements);
	myQueue.makeEmpty();
	myQueue.enqueue(*root);
	while(!myQueue.isEmpty())
	{
		CharNode node = myQueue.dequeue();
		if(node.isDefined)
			cout << "Char: " << node.c << "\tBinaryLocation: " << node.binLoc << endl;
		else
			cout << "Internal Node\tLocation: " << node.binLoc << endl;
		if(node.left != NULL)
			myQueue.enqueue(*(node.left));
		if(node.right != NULL)
			myQueue.enqueue(*(node.right));
	}
}//LOprintTrie

/*
	Prints sorted heap
*/
void HuffmanTrie::printSortedHeap()
{   
	  //while the heap isn't empty, sort and print
	while(!heap.isEmpty())
	{
		cout << heap.findMin().c << "\t" << heap.findMin().frequency << endl;
		heap.deleteMin();
	}//while
}//printSortedHeap

/*
	Prints the trie
*/
void HuffmanTrie::printTrie(CharNode const * node)
{
	//Base Case
	if(node)
	{
		printTrie(node->left);
		if(node->isDefined)
			cout << "Char: " << node->c << "\tLocation: " << node->binLoc << endl;
		else
			cout << "Internal Node\tLocation: " << node->binLoc << endl;
		printTrie(node->right);
	}//if
}//printTrie

/*
	Sets the binary equivalents of all the nodes
*/
void HuffmanTrie::setBinaryLocation(CharNode const * node)
{
	if(node)
	{
		if(node->left)
		{
			//node->left->binLoc <<= 1;
			shiftBitsLeft(node->left);
			setBinaryLocation(node->left);
		}//if

		if(node->right)
		{
			//node->right->binLoc <<= 1;
			//node->right->binLoc |= 1;
			shiftLeftAndOr(node->right);
			setBinaryLocation(node->right);
		}//if
	}//if
}//setBinaryLocation

/*
	Shifts all the binLoc's in the tree left 1
*/
void HuffmanTrie::shiftBitsLeft(CharNode * node)
{
	if(node)
	{
		shiftBitsLeft(node->left);

		node->binLoc <<= 1;
		node->numBits++;

		shiftBitsLeft(node->right);
	}//node
}//shiftBitsLeft

/*
	Shifts all the binLoc's in the tree left and or's them with 1
*/
void HuffmanTrie::shiftLeftAndOr(CharNode * node)
{
	if(node)
	{
		shiftLeftAndOr(node->left);

		node->binLoc <<= 1;
		node->binLoc |= 1;
		node->numBits++;

		shiftLeftAndOr(node->right);
	}
}//shiftLeftAndOr

/*
	Update the binLocs in the hash table
*/
void HuffmanTrie::updateHashBinLoc(CharNodeHash * hash, CharNode const * node)
{
	if(node)
	{
		updateHashBinLoc(hash, node->left);

		if(node->isDefined)
		{
			hash->updateCharNode(node->c, node->binLoc, node->numBits);
		}

		updateHashBinLoc(hash, node->right);
	}//if
}//updateHashBinLoc



